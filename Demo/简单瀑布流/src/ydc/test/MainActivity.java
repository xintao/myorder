package ydc.test;

import ydc.test.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

public class MainActivity extends Activity {
    protected static final String TAG = "ArcHFSearchResult";
    private ScrollView svResult;
    private LinearLayout llItem;
    private int[] arrayImages={
		R.drawable.bamboo,
		R.drawable.beanmilk,
		R.drawable.beef,
		R.drawable.beefliver,
		R.drawable.beer,
		R.drawable.brownsuger,
		R.drawable.celery,
		R.drawable.chicken,
		R.drawable.clam,
		R.drawable.crab,
		R.drawable.dog,
		R.drawable.donkey,
		R.drawable.duck,
		R.drawable.egg,
		R.drawable.foreignonion,
		R.drawable.garlic,
		R.drawable.goose,
		R.drawable.honey,
		R.drawable.ic_launcher,
		R.drawable.lamb,
		R.drawable.leek,
};
    private int pageCount = 0;
    private int resultCount;
    private int eachCount = 10;
    private View view;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        svResult = (ScrollView) findViewById(R.id.arc_hf_search_result);
        llItem = (LinearLayout) findViewById(R.id.arc_hf_search_item);
        svResult.setOnTouchListener(svListener);
        view = svResult.getChildAt(0);
        resultCount = arrayImages.length;
        AddResult();
    }

    class svTouchListener implements OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                break;
            case MotionEvent.ACTION_UP:
                // 如果触发监听事件，并且有内容，并且ScrollView已经拉到底部，加载一次数据
                if (svListener != null
                        && view != null
                        && view.getMeasuredHeight() - 20 <= svResult
                                .getScrollY() + svResult.getHeight()) {
                    AddResult();
                }
                break;
            default:
                break;
            }
            return false;
        }
    }

    svTouchListener svListener = new svTouchListener();

    /**
     * 添加结果
     */
    protected void AddResult() {
    	Toast.makeText(MainActivity.this, "正在加载", Toast.LENGTH_SHORT)
		.show();
        if (eachCount * pageCount < resultCount) {
            for (int i = 0; i < eachCount; i++) {
                int k = i + eachCount * pageCount;
                if (k >= resultCount)
                    break;
                ImageView tv = new ImageView(this);
                tv.setImageResource(arrayImages[k]);//("hello world" + arrayImages[k]);
                llItem.addView(tv);
            }
            pageCount++;
            Toast.makeText(MainActivity.this, "加载完成", Toast.LENGTH_SHORT)
			.show();
        }else {
        	//加载完了没有更多了
        	Toast.makeText(MainActivity.this, "加载完了", Toast.LENGTH_SHORT)
			.show();
        }
    }
}